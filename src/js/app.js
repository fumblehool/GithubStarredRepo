import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router';
import Header from './Header.js';
import MasterPage from './MasterPage.js';
import LoginPage from './LoginPage.js';

ReactDOM.render(
    <Router history={browserHistory}>
        <Route path='/' component={MasterPage}>
            <IndexRoute component={Header} />
            <Route path='/login' component={LoginPage} />
            <Route path='/Header' component={Header} />
        </Route>
    </Router>
    ,
    document.getElementById('app')
);