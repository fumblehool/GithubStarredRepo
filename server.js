var express = require('express');
var path = require('path');

var app = new express();

app.use(express.static(__dirname));

app.get('/', function(req, res){
    res.sendFile(path.join(__dirname + '/src/html/index.html'));
}).listen(3000);

var githubOAuth = require('github-oauth')({
    githubClient: process.env.CLIENT,
    githubSecret: process.env.SECRET,
    baseURL: 'http://localhost:3000',
    loginURI: '/login',
    callbackURI: '/callback',
    scope: 'user' // optional, default scope is set to user 
})
 
app.get('/login', function(req, res) {
    return githubOAuth.login(req, res);
})
 
app.get('/callback', function(req, res) {
    return githubOAuth.callback(req, res)
})
githubOAuth.on('error', function(err) {
  console.error('there was a login error', err)
})
 
githubOAuth.on('token', function(token, serverResponse) {
  console.log('here is your shiny new github oauth token', token)
  serverResponse.end(JSON.stringify(token))
})