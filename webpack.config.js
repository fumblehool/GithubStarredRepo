var webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: {
        app: './src/js/app'
    },

    output: {
        path: path.join(__dirname, 'build'),
        filename: '[name].min.js'
    },

    resolve: {
        extensions: ['', '.js', '.jsx', '.json']
    },

    module: {
        loaders: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel'
        }]
    }
};